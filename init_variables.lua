function init_variables()
    --set transparency colors
    palt(0,false)
    palt(6,true)
    --coeff movements
    con=1 --for normal moves
    cod=.8 --for diag moves

    bck=6 -- background color

    bump_coeff=2
    bump_timer=10

    aboxes={}
end

function init_actors()
    actors={}
end

function init_player()
    p={
        type="player",
        sp=17,
        x=64,
        y=64,
        dx=0,
        dy=0,
        w=8,
        h=8,
        pv=50,
        box={x1=0,y1=0,x2=7,y2=7},
        ldir={0,-1}, -- last dir
        lrdir={0,-1}, -- last rectilign dir
        bdir={0,0}, -- bumped direction
        attack={
            type="cac",
            dmg=10
        },
        attacking=false,
        input_attack=false,
        input_attack_pressed=0,
        touched=false,
        bumped=false,
        bumped_timer=0,
        dashing=false
    }
    actors[1]=p
end

function create_actor(t,x,y)
    local a={
        type=t,
        sp=4,
        x=x,
        y=y,
        cx=0, -- center coords (initiate 0)
        cy=0,
        dx=0,
        dy=0,
        w=8,
        h=8,
        pv=100,
        box={x1=0,y1=0,x2=7,y2=7},
        ldir={0,0},
        lrdir={0,0},
        bdir={0,0},
        attack={type="cac",dmg=10},
        attacking=false,
        touched=false,
        bumped=false,
        bumped_timer=0,
        dashing=false
    }
    return a
end

function init_attack_box(a)
    local b={
        x=0,
        y=0,
        cx=0,
        cy=0,
        box={x1=0,y1=0,x2=7,y2=7},
        dir={0,0},
        owner=a,
        timer=4,
    }
    return b
end