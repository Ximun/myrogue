function actors_draw()
    for a in all(actors) do
        spr(a.sp,a.x,a.y,1,1,false,false)
    end
end

function attacks_draw()
    if #aboxes>0 then
        for b in all(aboxes) do
            rectfill(b.x,b.y,b.x+b.box.x2,b.y+b.box.y2,8)
        end
    end
end