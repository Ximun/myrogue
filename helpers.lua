function is_in(elem,array)
    for k, v in ipairs(array) do
        if elem == v then
            return true
        end
    end
    return false
end